<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="shortcut icon" href="<?php Template::src("resize/admin/images/favicon_1.ico"); ?>">
	<title><?php Template::title(); ?></title>
	<?php
		Template::style("css");
	?>

  <!-- GOOGLE FONTS -->
  <link href='//fonts.googleapis.com/css?family=Raleway:400,700,600,800%7COpen+Sans:400italic,400,600,700' rel='stylesheet' type='text/css'>

  <!--[if IE 9]>
    <script src="js/media.match.min.js"></script>
  <![endif]-->

</head>
    <body>
<div id="main-wrapper">
        <!-- Header -->
        <?php Layout::extend("header"); ?>
        
        <!-- Content -->
        <?php Layout::extend("content"); ?>
		
        <?php Layout::extend("misc"); ?>
            
        <!-- Footer -->
        <?php Layout::extend("footer"); ?>
    </div>
	
</div> <!-- end #main-wrapper -->
	<?php Template::script(); ?>
</body>
</html>