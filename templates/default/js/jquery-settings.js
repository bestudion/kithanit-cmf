$(document).ready(function(){
    console.log("{$name} Template");
    
    $(".dropdown-toggle").dropdown();
    
    $(".scrollbox").enscroll({
        addPaddingToPane: false,
        pollChanges: true,
        showOnHover: true,
        minScrollbarLength : 300,
        zIndex: 50000,
        verticalTrackClass: "track3",
        verticalHandleClass: "handle3"
    });
    
    $(".resizable").resizable({
        ghost: true,
        autoHide: true,
        minWidth: 400,
        // grid: [ 20, 10 ],
        aspectRatio: false
    });
});