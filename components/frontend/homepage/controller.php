<?php

// This is our controller, handling all the logic. See libraries/controller.php
class HomepageController
{
    public static function build(){
        Template::title(array("pattern"=>"%s | %s"));
        Template::title("Welcome");
    }
    
    public function indexAction()
    {
        /*
        $view = new View('index');
        $model = Model::create('helloworld');

        $message = $model->getMessage();

        $view->set('message', $message);
        $view->render();
        */
    }

    public function nameAction($arguments)
    {
            $view = new View('name');

            if(count($arguments) > 0)
                    $view->set('name', $arguments[0]);
            else
                    $view->set('name', 'John Doe');

            $view->render();
    }
}

?>