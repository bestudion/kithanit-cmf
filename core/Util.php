<?php
    function generateUID()
    {
        return strtoupper(uniqid());
    }
    function redirectURL($url)
    {
        header('Location: '.$url);
    }
    
    function listDirectory($dir, $filter = '')
    {
        $ite=new RecursiveDirectoryIterator($dir);
        $tmp = array();
        foreach (new RecursiveIteratorIterator($ite) as $filename=>$cur) {
            
            if(!empty($filter))
            {
                if(strpos($filename, $filter) !== false){} else
                array_push($tmp, $filename);
            }
            else
            array_push($tmp, $filename);
            
        }
        
        return $tmp;
    }
    
    
    function cms_error($error)
    {
        $path = $_SERVER['DOCUMENT_ROOT'].'/cms_error.log';

        $data = file_get_contents($path, true);

        $error = "\r\n".'['.date('Y-m-d H:i').']'.$_SERVER['SCRIPT_FILENAME'].' on line: '.__LINE__.' - ERROR:  '.$error.''."\r\n";

        WriteFile($path, $data.$error);
    }
    function sql_error($error)
    {
        $error = str_replace('<br/>', "\r\n", $error);
        $error = strip_tags($error);

        $path = $_SERVER['DOCUMENT_ROOT'].'/sql_error.log';
        
        if(!fileexists($path))
        WriteFile($path, '');
        
        $data = file_get_contents($path, true);

        $debug = debug_backtrace();
        
        $error = "\r\n"."[".date('Y-m-d H:i').'] '.$debug[2]['file'].' on line: '.$debug[2]['line'].'- SQL_ERROR: '.$error ."\r\n";

        WriteFile($path, $data.$error);
            
    }
    function format_url($str)
    {
            $s = array(' ', '?', '=', '#', '$', '%', '&', '*', '(', ')', ',', '+', '\'', '"', '\\', '/', '<', '>');
            $r = array('-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
            return str_replace($s,$r,strtolower(remove_accents($str)));
    }
    function Capitalize($str)
    {
        $exp = explode(' ', $str);
        $ret = '';
        
        for($i=0;$i<sizeof($exp);$i++)
        {
            $str = $exp[$i];
            $ret .=  strtoupper(substr($str, 0, 1)).substr($str, 1, strlen($str)-1).' ';	
        }
        
        return $ret;
    }
    function getParam($url, $pn)
    {
        $u = explode('&', $url);
        
        for($i=0;$i<sizeof($u);$i++)
        {
            $p = explode('=', $u[$i]);
            $pname = strtolower($p[0]);
            $pval = $p[1];
            
            if($pname == $pn)
                return $pval;
        }
        
        return "";
    }
    
    
    function createWatermarkedImage($originalFileContents, $watermark, $originalWidth, $originalHeight, $paddingFromBottomRight = 0, $display = false, $watermark_text = '', $expiration_date = '') {
        $watermarkFileLocation = $watermark;
        $watermarkImage = imagecreatefrompng($watermarkFileLocation);
        $watermarkWidth = imagesx($watermarkImage);  
        $watermarkHeight = imagesy($watermarkImage);

        $originalImage = imagecreatefromjpeg($originalFileContents); //900x540

        //$destX = $originalWidth - $watermarkWidth - $paddingFromBottomRight;  
        //$destY = $originalHeight - $watermarkHeight - $paddingFromBottomRight;
        
        /*
        $pad_x = 100;
        $pad_y = 100;
        
        $destX = 90+$pad_x;
        $destY = 205+$pad_y;
        
        $tdestX = 10;
        $tdestY = 190+$pad_y;
        
        $edestX = 238;
        $edestY = 246;
        
        $font= "fonts/arial_narrow_7.ttf";
        $fontsize="23";
        $efontsize="18";
        */
        $pbottom = 0;
        $destX = 205; //vonalod
        $destY = 522;
        
        $tdestX = 105;
        $tdestY = 489;
        
        $edestX = 407;
        $edestY = 416;
        
        $font= "fonts/arial_narrow_7.ttf";
        $fontsize="30";
        $efontsize="25";
        
        
        $white = imagecolorallocate($originalImage, 0, 0, 0);
        $font_size = 1; 
        $txt_max_width = intval(0.8 * $originalWidth);

        do {

            $font_size++;
            $p = imagettfbbox($font_size,0,$font,$watermark_text);
            $txt_width=$p[2]-$p[0];

        } while ($txt_width <= $txt_max_width);
        
        list($left,, $right) = imageftbbox( 12, 0, $font, $watermark_text);

        $textWidth = $right - $left;
        
        $tdestX = (($originalWidth - $textWidth) / 2);
        
        // creating a cut resource
        $cut = imagecreatetruecolor($watermarkWidth, $watermarkHeight);
        
        // copying that section of the background to the cut
        imagecopy($cut, $originalImage, 0, 0, $destX, $destY, $watermarkWidth, $watermarkHeight);

        // placing the watermark now
        imagecopy($cut, $watermarkImage, 0, 0, 0, 0, $watermarkWidth, $watermarkHeight);

        // merging both of the images
        imagecopymerge($originalImage, $cut, $destX, $destY, 0, 0, $watermarkWidth, $watermarkHeight, 100);

        $watermarktext=$watermark_text;
        

        $text_new = iconv("UTF-8", "ISO-8859-2", $watermarktext);

        imagettftext($originalImage, $fontsize, 0, $tdestX-40, $tdestY, $white, $font, $text_new);
        imagettftext($originalImage, $efontsize, 0, $edestX, $edestY, $white, $font, $expiration_date);

        if($display)
        {
            header('Content-type: image/jpeg');
            imagejpeg($originalImage, NULL, 100);
            imagedestroy($originalImage);
        }
        else
        {
            imagejpeg($originalImage, $originalFileContents, 100);
            imagedestroy($originalImage);
        }
    }
    function CreateThumb($src_folder, $thumb_folder, $img, $new_img, $w, $h)
    {
        $image = new Resize_Image;

        $image->new_width = $w;
        $image->new_height = $h;


        $image->image_to_resize = strtolower($src_folder.'/'.$img); 
        $image->ratio = true; 
        $image->new_image_name = substr($new_img, 0, -4);
        $image->save_folder = strtolower($thumb_folder).'/';
        $process = $image->resize();
        if($process['result'] && $image->save_folder)
        {
        //echo 'The new image ('.$process['new_file_path'].') has been saved.';
        }
        else
        die( "ERROR: creating thumbnail.");

     }
     function downloadImage($source, $dest, $display = false)
    {
        //file_put_contents($dest, file_get_contents($source));

        $ch = curl_init($source);
        $fp = fopen($dest, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        if($display)
        {
            header('Content-type: image/png');
            $dst = imagecreatefrompng($dest);
            imagepng($dst);
            imagedestroy($dst);
        }

        return $dest;
    }

    
    function rarray( $arr ){
        $return="";
        foreach( $arr as $key=>$value ){
            $return .= $key . " = " . $value . ";";
        }
        return $return;
    }
    function parray( $cont ){
        echo "<pre>";
            print_r($cont);
        echo "</pre>";
    }
    function dump( $cont ){
        echo "<pre>";
            var_dump($cont);
        echo "</pre>";
    }

    function class_in_file($class, $file){
        $classes = $nsPos = $final = array();
        $foundNS = FALSE;
        $ii = 0;
        
        if (!fileexists($file) || !has($file, "." ) ) return NULL;
        
        $php_code = file_get_contents($file);
        $tokens = token_get_all($php_code);
        $count = count($tokens);

        for ($i = 0; $i < $count; $i++) 
            if ($i-2 >= 0 && $tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING)
                $fclass = $tokens[$i][1];
        
        if (empty($fclass)) return NULL;

        return $class == $fclass;
    }
    
    function reverse_prettyurl($string){
        $search = array('-', '_', '-and-', 'and');
        $replace = array(' ', ' ', ' ', ' ');

        $string = str_replace($search, $replace, $string);
        $string = ucfirst($string);
        
        return $string;
    }

    function array_add($itemtoadd, $array) {
        foreach ($array as &$element)
            $element = $itemtoadd . $element;
        return $array;
    }

    function printf_array($format, $arr){ 
            ob_start();
            call_user_func_array('printf', array_merge((array)$format, $arr));
            $return = ob_get_contents();
            ob_end_clean();
            return $return;
    }

    function split_nth($str, $delim, $n){
            return array_map(function($p) use ($delim) {
                    return implode($delim, $p);
            }, array_chunk(explode($delim, $str), $n));
    }

    function stringSplit($string, $search, $chunck) {
        return array_map(function($var)use($search){return implode($search, $var); },array_chunk(explode($search, $string),$chunck));
    }
    function split_nth2($string,$needle,$nth){
        $max = strlen($string);
        $n = 0;
        for($i=0;$i<$max;$i++){
            if($string[$i]==$needle){
                $n++;
                if($n>=$nth){
                    break;
                }
            }
        }
        $arr[] = substr($string,0,$i);
        $arr[] = substr($string,$i+1,$max);

        return $arr;
    }

    function scriptname(){
            $req = explode("/", $_SERVER["SCRIPT_NAME"]);
            $req = end($req);
            $req = explode(".",$req);
            $req = $req[0];

            //return $req;
            return $_SERVER["SCRIPT_NAME"];
    }

    function NOW(){
            return date("Y-m-d H:i:s", time());
    }

    function writefile($file, $content, $perm="a+"){
        $folder = pathinfo($file);
        $folder = $folder["dirname"];
        if( !folderexists($folder) ) mkdir($folder, 0777, true);
        
        if( file_put_contents($file, $content) )
            return true;
        else
            return false;
        
        
        /*
        if (!$fp = fopen($file, $perm))
                return "couldnotopen";

        if (!fwrite($fp, $content))
                return "couldnotread";

        fclose($fp);
        */
    }
    function remote_fileexists($url){
       return(bool)preg_match('~HTTP/1\.\d\s+200\s+OK~', @current(get_headers($url)));
    }
    
    function downloadFile($url, $newfile, $callback=null, $optional=null){
        $remotefile = file_get_contents($url);
        
        $callbacks = str_replace(" ", "", $callback);
        
        $callbacks = explode(",", $callback);
        
        foreach( $callbacks as $key=>$callback ){
            if( has($callback, "::")  ){
                $callback = explode("::", $callback);

                if( is_array($callback) && class_exists($callback[0]) && method_exists($callback[0], $callback[1]) )
                    $remotefile = $callback[0]::$callback[1]( $remotefile );

            }
            else{
                if( function_exists($callback) )
                    $remotefile = $callback($remotefile);
            }
        }
        
        cache_write($newfile, $remotefile, $optional);
        
        return $remotefile;
    }

    function hasTags( $str ){
        return !(strcmp( $str, strip_tags($str ) ) == 0);
    }

    function is_hex($hex_color){
        if(preg_match('/^[a-f0-9]{3,6}$/i', $hex_color)){
                return $hex_color;
        }
        else{
                return false;
        }
    }

    function has($which,$what){
            $which = strtolower($which);
            $what = strtolower($what);
            
            if( !$which || !$what ) return;

            if (strpos($which,$what) !== false) {
                    return true;
            }
            else{
                    return false;
            }
    }
    
    function array_has($array, $what, $rkey=null){
        if( $array === null || !is_array($array) ) return;
        $k=-1;
        $return = false;
        
        foreach( $array as $key=>$value ){
            $k++;
                
            if( !$return ){
                if( !is_array($value) && has($value, $what) ){
                    $return["index"] = $k;
                    $return["key"] = ($rkey) ? $rkey : $key;
                    $return["value"] = $value;
                }
                elseif( !is_array($key) && has($key, $what) ){
                    $return["index"] = $k;
                    $return["key"] = ($rkey) ? $rkey : $key;
                    $return["value"] = $value;
                }
                elseif( is_array($value) ){
                    $return = array_has($value,$what,($rkey)?$rkey:$key);
                }
                elseif( is_array($key) ){
                    $return = array_has($key,$what,($rkey)?$rkey:$key);
                }
            }
        }
        
        return $return;
    }

    function escape($str){ 
            $searchcode=array("<script>","<script type=\"text/javascript\">","<style type=\"text/css\">","<?php","<meta","redirect","refresh","?>","</script>","</style>","</html>");

            $replacecode=array("","","","","","","","","","","","");

            return str_replace($searchcode,$replacecode,$str);
    }

    function mysql_escape($str){
            return escape(mysql_real_escape_string($str));
    }

    function password($str){
            return escape(mysql_real_escape_string($str));
    }

    function url(){ 
            $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
            $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
            $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
            return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
    } 

    function domain(){ 
            $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
            $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
            $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
            return $protocol."://".$_SERVER['SERVER_NAME'].$port; 
    } 
    function strleft($s1, $s2) { 
            return substr($s1, 0, strpos($s1, $s2)); 
    }

    function data_img($image){
        $imageData = file_get_contents($image);
        $imageData = base64_encode($imageData);

        // Format the image SRC:  data:{mime};base64,{data};
        $src = "data:" . mime_content_type($image) . ";base64," . $imageData;

        return $src;
    }
    
    function imageFromText($text, $imagewidth, $imageheight, $fontangle, $textcolor, $backgroundcolor, $font, $fontsize, $config){
            ob_start();
            $font = $_SERVER["DOCUMENT_ROOT"].$config["Template"]["Paths"]["assets"].$config["Template"]["Paths"]["font"].$font.".ttf";
            ### Convert HTML backgound color to RGB
            if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $backgroundcolor, $bgrgb ) )
            {$bgred = hexdec( $bgrgb[1] );   $bggreen = hexdec( $bgrgb[2] );   $bgblue = hexdec( $bgrgb[3] );}

            ### Convert HTML text color to RGB
            if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $textcolor, $textrgb ) ){
                    $textred = hexdec( $textrgb[1] );   $textgreen = hexdec( $textrgb[2] );   $textblue = hexdec( $textrgb[3] );
            }

            ### Create image
            $im = imagecreate( $imagewidth, $imageheight );

            ### Declare image's background color
            $bgcolor = imagecolorallocate($im, $bgred,$bggreen,$bgblue);

            ### Declare image's text color
            $fontcolor = imagecolorallocate($im, $textred,$textgreen,$textblue);

            ### Get exact dimensions of text string
            $box = @imageTTFBbox($fontsize,$fontangle,$font,$text);

            ### Get width of text from dimensions
            $textwidth = abs($box[4] - $box[0]);

            ### Get height of text from dimensions
            $textheight = abs($box[5] - $box[1]);

            ### Get x-coordinate of centered text horizontally using length of the image and length of the text
            $xcord = ($imagewidth/2)-($textwidth/2)-2;

            ### Get y-coordinate of centered text vertically using height of the image and height of the text
            $ycord = ($imageheight/2)+($textheight/2);

            ### Declare completed image with colors, font, text, and text location
            imagettftext ( $im, $fontsize, $fontangle, $xcord, $ycord, $fontcolor, $font, $text );

            ### Display completed image as PNG
            imagepng($im);

            ### Close the image
            imagedestroy($im);

            $imageintovar = ob_get_clean();

            return $imageintovar;
    }

    function timeAgo($date){

            if(empty($date)) {

            return "No date provided";

            }

            $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");

            $lengths = array("60","60","24","7","4.35","12","10");

            $now = time();

            $unix_date = strtotime($date);

            // check validity of date

            if(empty($unix_date)) {

            return "Bad date";

            }

            // is it future date or past date

            if($now > $unix_date) {

            $difference = $now - $unix_date;

            $tense = "ago";

            } else {

            $difference = $unix_date - $now;
            $tense = "from now";}

            for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {

            $difference /= $lengths[$j];

            }

            $difference = round($difference);

            if($difference != 1) {

            $periods[$j].= "s";

            }

            return "$difference $periods[$j] {$tense}";
    }    
    
    function filesize_formatted($file)
    {
        $bytes = filesize($file);

        if ($bytes >= 1073741824) {
            return number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            return number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            return number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            return $bytes . ' bytes';
        } elseif ($bytes == 1) {
            return '1 byte';
        } else {
            return '0 bytes';
        }
    }
    
    function gzip_compression() {
        //If no encoding was given - then it must not be able to accept gzip pages
        if( empty($_SERVER['HTTP_ACCEPT_ENCODING']) ) { return false; }

        //If zlib is not ALREADY compressing the page - and ob_gzhandler is set
        if (( ini_get('zlib.output_compression') == 'On'
            OR ini_get('zlib.output_compression_level') > 0 )
            OR ini_get('output_handler') == 'ob_gzhandler' ) {
            return false;
        }

        //Else if zlib is loaded start the compression.
        if ( extension_loaded( 'zlib' ) AND (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE) ) {
            ob_start('ob_gzhandler');
        }
    }

    function compress_image($source_url, $destination_url, $quality) {
		if( filesize( $source_url ) == 0 ) return;
		
        $info = getimagesize($source_url);
        $quality = ( $info["mime"] == "image/jpeg" ) ? $quality : 10-(int)($quality/10);

        if ($info["mime"] == "image/jpeg") $image = imagecreatefromjpeg($source_url);
        elseif ($info["mime"] == "image/gif") $image = imagecreatefromgif($source_url);
        elseif ($info["mime"] == "image/png") $image = imagecreatefrompng($source_url);
        elseif ($info["mime"] == "image/png") $image = imagecreatefrompng($source_url);
        else  $image = file_get_contents($source_url);

        $pathinfo = pathinfo($destination_url);
        $dir = $pathinfo["dirname"];
        
        if( !is_dir($dir) )
            mkdir($dir, 0777, true);
        
        // if image mime type not correct, redo it
        /*
        if( cache_exists($destination_url) && getimagesize($destination_url)["mime"] != $info["mime"] )
            unlink($destination_url);
        */
        
        //save file
        if( !is_file($destination_url) ){
            if ($info["mime"] == "image/jpeg") imagejpeg($image, $destination_url, $quality);
            elseif ($info["mime"] == "image/png") imagepng($image, $destination_url, $quality);
            elseif ($info["mime"] == "image/gif") imagegif($image, $destination_url, $quality);
            else file_put_contents($destination_url, $image);
        }
        // imagedestroy($image);
        
        // dump( getimagesize($destination_url) ); exit();

        $file = $source_url;
        $pathinfo = pathinfo($file);
        $Fullpath = $pathinfo["dirname"] ."/".$pathinfo["basename"];
        $query = "SELECT `ID`, `Filesize`, `Skip` FROM `cache` WHERE `Fullpath`='".$Fullpath."'";
        $query = Query::create($query);
        $query = Query::getResults();
        $ID = ( isset($query["ID"]) ) ? $query["ID"] : 0;
        $Filesize = ( isset($query["Filesize"]) ) ? (int)$query["Filesize"] : 0;
        $DBSkip = ( isset($query["Skip"]) ) ? $query["Skip"] : "no";
        $File = $pathinfo["basename"];
        $Filename = $pathinfo["filename"];
        $Layout = Builder::$layout;
        $WillSkip = ( cache_skip($Fullpath) ) ? "yes" : "no";
        // every single css which is not style will be cached only once.
        $WillSkip = ( $pathinfo["extension"] == "css" && Resources::$library != "less" ) ? "yes" : $WillSkip;
        
        $dirname = str_replace(Builder::$root, "", $Fullpath);
        
        $cachefile = fixpath(Builder::$cache_path."/".$dirname);
        
        /*
        var_dump( "Filename  : " . $Filename );
        var_dump( "cache exists : " . cache_exists($file) );
        var_dump( "skip " . $WillSkip );
        var_dump( "Skip DB " . $DBSkip );
        */
        
        if( $DBSkip != "yes" && cache_skip($cachefile) || !cache_exists($cachefile) ){
        // check if have to skip, if the file does not exist, attempt to write in file, if succeeded store the info in the db...
            $Filesize = cache_size($file);
            $Timenow = date("Y-m-d H:m:s", time());
            $originalFile = $source_url;
            $Folder = $pathinfo["dirname"];
            
            $query = "REPLACE INTO `cache`";
            if( $ID == 0 ){
                $query .= "(`ID`, `Layout`, `File`, `Skip`, `Filesize`, `Filename`, `Original`, `Folder`, `Fullpath`, `Created`) VALUES";
                $query .= "('".$ID."','".$Layout."','".$File."','".$WillSkip."','".$Filesize."','".$Filename."','".$originalFile."','".$Folder."','".$Fullpath."','".$Timenow."')";
            }
            else{
                $query .= "(`ID`, `Layout`, `File`, `Filesize`, `Filename`, `Original`, `Folder`, `Fullpath`, `Modified`) VALUES";
                $query .= "('".$ID."','".$Layout."','".$File."','".$Filesize."','".$Filename."','".$originalFile."','".$Folder."','".$Fullpath."','".$Timenow."')";
            }
            
            Query::create($query);
        }
        
            
        //return destination file
        return $destination_url;
    }
    
    
    function is_url($url) {
        $return = parse_url($url);
        
        if( isset($return["scheme"]) || isset($return["host"]) )
            $return = true;
        else
            $return = false;
        
        return $return;
    }
    
    function http_response($url, $which=null){
        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1) ;
        $content = curl_exec($ch);
        $return=null;
        if(!curl_errno($ch))
        {
                $info = curl_getinfo($ch);
                
                
                if( isset($info[$which]) )
                    $return = $info[$which];
                else
                    $return = $info;
                
                // $info = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
                
        }
        curl_close($ch);
        
        return $return;
    }

    function cache_modtime($file){
        $pathinfo = pathinfo($file);
        $Fullpath = $pathinfo["dirname"] ."/".$pathinfo["basename"];
        $filemtime = 0;
        
        
        $Fullpath = $pathinfo["dirname"] ."/".$pathinfo["basename"];
        $query = "SELECT `Modified` FROM `cache` WHERE `Fullpath`='".$Fullpath."'";
        $query = Query::create($query);
        $query = Query::getResults();
        $filemtime = ( isset($query["Modified"]) ) ? $query["Modified"] : 0;
        $filemtime = strtotime($filemtime);
        
        return $filemtime;
    }
    
    function cache_exists($file){
        $return = ( $file ) ? realpath( $file ) : false;
        
        return $return;
    }
    
    function fileexists($file){
        $return = ( $file ) ? realpath( $file ) : false;
        
        return $return;
    }
    function folderexists($file){
        $return = ( $file ) ? realpath( $file ) : false;
        
        return $return;
    }
    
    function explodeit($what, $string, $iterator){
        $return = explode($what, $string);
        $return = ( is_array($return) && isset($return[$iterator]) ) ? $return[$iterator] : false;
        
        
        return $return;
    }
    
    function returnit($array, $iterator){
        $return = ( is_array($array) && isset($array[$iterator]) ) ? $array[$iterator] : false;
        
        return $return;
        
    }
    
    function cache_skip( $toskip=null ){
        $skip = cache_skiplist();
        $toskip = ( $toskip ) ? pathinfo($toskip) : null;
        
        $extension = ( isset($toskip["extension"]) ) ? $toskip["extension"] : null;
        $filename = ( isset($toskip["filename"]) ) ? $toskip["filename"] : null;
        $dirname = ( isset($toskip["dirname"]) ) ? $toskip["dirname"] : null;
        $dirname = str_replace(Builder::$root, "", $dirname);
        $dirname = ( count(explode("/", $dirname)) > 1 ) ? explodeit("/", $dirname, 1) : $dirname;
        $dirs = ( $dirname ) ? explode("/",$dirname) : array();
        $lastdir = end($dirs);
        $basename = ( isset($toskip["basename"]) ) ? $toskip["basename"] : null;
        $dirnfile = $lastdir ."/". $basename;
        
        if( in_array($dirnfile, $skip) || in_array($basename, $skip) || in_array($filename, $skip) || in_array($extension, $skip) || in_array($dirname, $skip) )
            return true;
        else
            return false;
    }
    
    function cache_skiplist(){
        $template = Template::$template_info;
        $always_skip = "jquery-settings,less/style.css,fonts,gen_preload";
        $cache_skip = ( isset($template["cache_skip"]) ) ? $template["cache_skip"] : "";
        // will use this when plugins will be extendable
        $plugins = ( 1==2 && isset($template["javascript"][0]["plugins"]) ) ? $template["javascript"][0]["plugins"] : "";

        $skip = $always_skip.",".$cache_skip .",".$plugins;
        $skip = str_replace(" ", "", $skip);
        $skip = array_filter(explode(",", $skip));
        
        return $skip;
    }
    
    function cache_size($file){
        $size = ( cache_exists($file) ) ? filesize($file) : null;
        
        return $size;
    }
    
    function cache_write($file, $content, $optional=null){
        $pathinfo = pathinfo($file);
        $Fullpath = $pathinfo["dirname"] ."/".$pathinfo["basename"];
        $query = "SELECT `ID`, `Filesize`, `Skip` FROM `cache` WHERE `Fullpath`='".$Fullpath."'";
        $query = Query::create($query);
        $query = Query::getResults();
        $ID = ( isset($query["ID"]) ) ? $query["ID"] : 0;
        $Filesize = ( isset($query["Filesize"]) ) ? (int)$query["Filesize"] : 0;
        $DBSkip = ( isset($query["Skip"]) ) ? $query["Skip"] : "no";
        $File = $pathinfo["basename"];
        $Layout = Builder::$layout;
        $Filename = $pathinfo["filename"];
        $WillSkip = ( cache_skip($Filename) ) ? "yes" : "no";
        // every single css which is not style will be cached only once.
        $WillSkip = ( $pathinfo["extension"] == "css" && Resources::$library != "less" ) ? "yes" : $WillSkip;
        
        /*
        var_dump( "Filename  : " . $Filename );
        var_dump( "cache exists : " . cache_exists($file) );
        var_dump( "skip " . $WillSkip );
        var_dump( "Skip DB " . $DBSkip );
        
        echo "\n\n";
        
        */
        // check if have to skip, if the file does not exist, attempt to write in file, if succeeded store the info in the db...
        if( $DBSkip != "yes" && writefile( $file, $content ) && cache_skip($file) || !cache_exists($file) ){
            $Filesize = cache_size($file);
            $Timenow = date("Y-m-d H:m:s", time());
            $originalFile = explodeit(Builder::$cache_path, $file, 1);
            $originalFile = Builder::$template_path . $originalFile;
            $Folder = $pathinfo["dirname"];
        
            $query = "REPLACE INTO `cache`";
            if( $ID == 0 ){
                $query .= "(`ID`, `Layout`, `File`, `Skip`, `Filesize`, `Filename`, `Original`, `Folder`, `Fullpath`, `Optional`, `Created`) VALUES";
                $query .= "('".$ID."','".$Layout."','".$File."','".$WillSkip."','".$Filesize."','".$Filename."','".$originalFile."','".$Folder."','".$Fullpath."','".$optional."','".$Timenow."')";
            }
            else{
                $query .= "(`ID`, `Layout`, `File`, `Filesize`, `Filename`, `Original`, `Folder`, `Fullpath`, `Optional`, `Modified`) VALUES";
                $query .= "('".$ID."','".$Layout."','".$File."','".$Filesize."','".$Filename."','".$originalFile."','".$Folder."','".$Fullpath."','".$optional."','".$Timenow."')";
            }
            
            Query::create($query);
            
            return true;
        }
        else
            return false;
    }
    
    if ( ! function_exists('glob_recursive'))
    {
        // Does not support flag GLOB_BRACE

        function glob_recursive($pattern, $flags = 0)
        {
            $files = glob($pattern, $flags);

            foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
            {
                $files = array_merge(glob_recursive($dir.'/'.basename($pattern), $flags), $files);
            }
            return $files;
        }
    }
    
    function fixpath($path){
        for($i=0;$i<10;$i++){
            $path = str_replace("../", "/", $path);
            $path = str_replace("//", "/", $path);
            $path = ( has($path, "?") ) ? explodeit("?", $path, 0) : $path;
        }
        
        return $path;
    }
    
    function encrypt($data) {
       $salt = SALT;
       $stringsize = 32;
       $key = substr(hash('sha256', $salt), 0, $stringsize);
       $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
       $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
       $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, serialize($data), MCRYPT_MODE_ECB, $iv));
       return $encrypted;
    }

    function decrypt($data) {
       $salt = SALT;
       $stringsize = 32;
       $key = substr(hash('sha256', $salt), 0, $stringsize);
       $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
       $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
       $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, (base64_decode($data)), MCRYPT_MODE_ECB, $iv);
       return $decrypted;
    }
    
    function is_image($path) {
        if ( fileexists($path) && getimagesize($path) && is_array(getimagesize($path)) ){
            return true;
        }
        
        return false;
    }
    
    function backtrace($return = false){
        $backtrace = debug_backtrace();

        if( $return == "array" )
            return $backtrace;
        elseif( $return = "pre" )
            return "<pre>".$backtrace."</pre>";
        else
            return print_r( $backtrace, $return);
    }
?>
