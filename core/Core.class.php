<?php

    class Core{
            static $root;
            
            public function __construct(){
                self::$root=$_SERVER["DOCUMENT_ROOT"];
                   
                $this->include_files("core", "php", "/core/Util.php", "core/components/");
                //$this->include_files("core/components/*/controller.php", "php");
                $this->include_files("libraries", "php");
            }
            public static function query($query)
            {
                $q = Query::create($query);
                
                return $q;
            }
            
            public static function isDev(){
                if( ENV == "production" || Router::isBot() )
                    return false;
                else 
                    return true;
            }
            
            public function include_files($directory, $filter_ext = "", $skip_files_list = "", $skip_folder_list = "")
            {
                $dir = self::$root."/".$directory;
                
                if(fileexists($dir) && is_dir($dir) || has($directory,"*"))
                {
                    $files = ( has($directory,"*") ) ? glob_recursive($directory) : listDirectory($dir, $skip_folder_list);
                    $self = basename(__FILE__);
                    $skip_files = explode(",",str_replace(" ,",",",strtolower($skip_files_list)));
					
                    for($i=0;$i<sizeof($files);$i++)
                    {

                        $ext = strtolower(pathinfo($files[$i], PATHINFO_EXTENSION));

                        $file = str_replace(self::$root, "", $files[$i]);

                        if($file[0] == "/")
                        $file = substr ($file, 1, strlen($file));

                        $set_filter = true;

                        if(!in_array(strtolower($file), $skip_files))
                        {
                            $set_filter = true;

                            $flist = explode("|", strtolower($filter_ext));

                            if(!empty($filter_ext))
                            {
                                $set_filter = in_array($ext, $flist);
                            }

                            if($set_filter)
                            {
                                if($ext == "css")
                                echo "<link href=\"".$this->server."/".$file."\" rel=\"stylesheet\" type=\"text/css\">";
                                else if($ext == "js")
                                echo "<script type=\"text/javascript\" src=\"".$this->server."/".$file."\"></script>";
                                else 
                                {
                                    $current_file = "core/".__CLASS__.".class.php";

                                    if($file != $current_file)
                                    {
                                      // echo "<br/>include: ".$files[$i];
                                       require_once $files[$i];
                                    }

                                }
                            }
                        }
                    }
                }
            }    

            public static function configJson(){
                $config_json = Builder::$root . "/config.json";
                $config_json = ( fileexists($config_json) ) ? json_decode( file_get_contents($config_json), true ) : null;

                return $config_json;
            }
            
            public static function component(){
                $component = Router::$controller;
                $layout = Layout::$current;
                $component_json = Builder::$components .$layout ."/" .$component. "/component.json";
                $component_json = ( fileexists($component_json) ) ? json_decode( file_get_contents($component_json), true ) : null;

                return $component_json;
            }

            public static function getSettings($name, $group = "")
            {
                $_GET["param"] = (isset($_GET["param"])) ? $_GET["param"] : "";
                $name = strtolower($name);
                $group = strtolower($group);
                $paramLayout = (isset(explode("/",$_GET["param"])[0])) ? explode("/",$_GET["param"])[0] : null;
				$layoutMaybe =  ( $paramLayout && Layout::layoutAlias($paramLayout) ) ? Layout::layoutAlias($paramLayout) : null;
				$layoutMaybe =  ( !$layoutMaybe ) ? $paramLayout : $layoutMaybe;
				$exists = ( $layoutMaybe && file_exists($_SERVER["DOCUMENT_ROOT"] . "/layout/".$layoutMaybe."/layout.php") ) ? $layoutMaybe : false;
				$exists = ( !$exists && isset(explode("/",$_GET["param"])[1]) && file_exists($_SERVER["DOCUMENT_ROOT"] . "/layout/".(explode("/",$_GET["param"])[1])."/layout.php") ) ? (explode("/",$_GET["param"])[1]) : $exists;
				
				$group = ( $exists ) ? $exists : "frontend";

                $config_json = Core::configJson();
                $domains = ( isset($config_json["domains"]) ) ? $config_json["domains"] : null;
                $domain = str_replace(array("https","http",":","/","//","80","443"),"",Router::$domain);
                $currentDomain = ( isset($domains[$domain]) ) ? $domains[$domain] : null;
                $layout = $group;
                // if we have set the domain as alias in config, we give'em it's settings
                if( is_array($currentDomain) && isset($currentDomain["layout"]) && array_has($currentDomain["layout"],$layout) ) $config_json = $currentDomain;


                if( isset($config_json["layout"]) ){
                    $layoutA = (isset($config_json["layout"])) ? $config_json["layout"] : null;
                    $layoutAlias = ( array_has($layoutA,$layout) ) ? array_has($layoutA,$layout)["key"] : null;
                    if( isset($layoutA[$layout][$name]) ){
                        $result = $layoutA[$layout];
                    }
                    else{
                        $result = ( isset($config_json["defaults"]) ) ? $config_json["defaults"] : null;
                    }

                    if( $result && is_array( $result ) ){
                        if( isset($result[$name]) ) return $result[$name];
                    }
                }

                // If not found in json go and try from db
                $SQL = "LOWER(`name`) = '$name'"; 
                if(!empty($group))
                    $SQL = " LOWER(`group`) LIKE '$group' ";

				$sql = "SELECT * FROM `settings` WHERE ".$SQL;
                $q = Query::create($sql);
                $res = Query::getResults();
				
                $tmp = array();

                if(!empty($group))
                {
                    if(count($res) == 1)
                    $res = array($res);

                    for($i=0;$i<sizeof($res);$i++)
                    {
                        $r = $res[$i];
                        $tmp[strtolower($r["name"])] = $r["value"];
                    }
                    
                    return $tmp[$name];
                }
                
                return $res["value"];
            }
            public static function getLangVal($name = "", $group = "", $return_array = false)
            {
                $name = strtolower($name);
                $group = strtolower($group);
                $SQL = "1";
                $tmp = array();

                if(!empty($name) || !empty($group))
                {
                    if(!empty($name))
                    {
                        $name = explode(",", $name); 

                        for($i=0;$i<sizeof($name);$i++)
                        {
                            $nm = str_replace(" ","",$name[$i]);

                            if(!empty($nm))
                            $names .= "OR LOWER(`name`) = '".strtolower($nm)."' ";
                        }

                        $SQL .= " AND (".substr($names, 2, strlen($names)).")";
                    }

                    if(!empty($group))
                        $SQL .= " AND LOWER(`group`) LIKE "%$group%"";

                    $q = new Query("SELECT * FROM `lang` WHERE ".$SQL);
                    $res = $q->getResults();

                    if($q->getRowsCount() == 1)
                    {

                        if($return_array)
                        return $res;

                        return $res["value"];
                    }

                    for($i = 0;$i<sizeof($res);$i++)
                    {
                        $r = $res[$i];

                        if($return_array)
                        array_push($tmp, $res[$i]);
                        else
                       $tmp[$r["name"]] = $r["value"];
                    }

                    return $tmp;    
                }

                return "";
            }

            public static function inject($file){
                // We define variables from the model, then pass to the layout
                $layout = returnit(pathinfo($file), "filename");
                if( $layout == "layout" ){
                    $appends = Layout::$append;
                    $prepends = Layout::$prepend;
                    Layout::include_smart($file, $appends, $prepends);
                }
                else{
                    Layout::include_smart($file);
                }
            }
            
        
    }

?>
