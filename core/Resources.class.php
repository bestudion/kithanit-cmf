<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Router
 *
 * Depends: Router, library called
 * @author Pityu
 */
class Resources {
        
    static $param, $path, $chunks, $library, $file, $extension, $folders, $current, $template, $folder;
    static $cachefile, $cachemodifiedtime, $fullfolder, $fullpath;
    
    public function __construct($path=null) {
        self::$param = Router::url("param");
        self::$path = ( $path ) ? $path : self::$param;
        
        self::$path = (self::$path) ? self::$path : "";
        self::$chunks = explode('/', self::$path);
        self::$chunks = array_values( array_filter(self::$chunks) );
        self::$folder = (self::$chunks[0] == "uploads" || cache_skip( self::getFile() ) && cache_skip( self::$path ) ) ? Builder::$root . "/" : Builder::$template_path;
    }
     
    public static function parse()
    {
        $libraries = Builder::$libraries;
        $ext=self::$chunks[0];
        self::$library = ( in_array($ext,$libraries)  ) ? array_shift(self::$chunks) : null;
        self::$file = array_pop(self::$chunks);
        self::$folders = ( count(self::$chunks) > 0 ) ? self::$chunks : array("");

        Builder::$layout = ( isset(self::$chunks[0]) && Layout::layoutExists(self::$chunks[0]) ) ? self::$chunks[0] : null;        
        Builder::$layout = ( !Builder::$layout && isset(self::$chunks[1]) && Layout::layoutExists(self::$chunks[1]) ) ? self::$chunks[1] : Builder::$layout;        

        if(  array_has(self::$folders,"uploads") ) { 
            $temp = self::$folders[0];
            self::$folders[0] = self::$folders[1];
            self::$folders[1] = $temp;
        }

        self::$extension = (self::$library == "less") ? "css" : self::getFileInfo("extension");
        
        self::$template = Template::parse();
    }
        
    public static function getLibrary(){
        return self::$library;
    }
    
    public static function getFolders($type=null){
        
        if( !$type )
            return implode("/", self::$folders) . "/";
        else
            return self::$folders;
    }
    
    public static function getFile(){
        $template = Template::parse();
        $jquery_version = (isset($template["javascript"])&&is_array($template["javascript"]))? $template["javascript"][0]["jQuery_version"] : null;
        
        if( self::$file == "jquery.js" )
            return "jquery-" . $jquery_version . ".js";
        else
            return self::$file;
    }
    
    public static function getFileInfo($wat=null){
        $file = self::$folder . self::getLibrary() ."/". self::getFolders() . self::getFile();
        $return = null;
        if( fileexists($file) ){
            $finfo = pathinfo($file);
            
            if( !$wat ) return $finfo;
                    
            if( in_array($wat, array_keys($finfo)) )
                $return = $finfo[$wat];
            elseif( $wat == "created" )
                $return = filectime($file);
            elseif( $wat == "modified" )
                $return = filemtime($file);
        }
        
        return $return;
    }

    public static function init(){
        // We parse the url so we can use it for determine the parts of it
        self::parse();
        $cfile = explode('/',self::getFolders());
        if(  array_has($cfile,"uploads") ) { 
            $temp = $cfile[0];
            $cfile[0] = $cfile[1];
            $cfile[1] = $temp;
        }        
        
        $cfile=implode("/",$cfile); 

        $tFolders = (explode("/",self::getFolders()));
        if( isset($tFolders[0]) && $tFolders[0] == Builder::$layout || in_array($tFolders[0],Builder::$libraries) ) array_shift($tFolders);

        $tFolders = implode("/", $tFolders);
        self::$fullfolder = ( is_dir( Builder::$templates_dir . Template::current() ."/".$tFolders ) ) ? Builder::$templates_dir."/".Template::current()."/". $tFolders : Builder::$root."/". self::getFolders();
        // self::$fullfolder = ( is_dir( Builder::$templates_dir . Template::current() ."/".$tFolders ) ) ? Builder::$templates_dir."/".Template::current()."/". self::getFolders() : Builder::$root."/". self::getFolders();
        $fullpath = self::$fullfolder ."/". self::getFile();
        
        self::$fullpath = str_replace("//", "/", $fullpath);
        $pathinfo = pathinfo(self::$fullpath);
        $ext = ( isset($pathinfo["extension"]) ) ? $pathinfo["extension"] : null;
        self::$cachefile = ( in_array(self::getLibrary(),array("css","less","resize","js")) || self::getLibrary() == Template::current() ) ? Builder::$cache_path .self::$library. "/". $cfile . self::$file : Builder::$cache_path . "/". $cfile . self::$file;
        self::$cachemodifiedtime = ( cache_modtime(self::$cachefile) ) ? time() - cache_modtime(self::$cachefile) : time();
        $library = self::getLibrary();
        $file = self::getFile();
        $folders = self::getFolders();

        $libraryClassName = self::getClassName($library);
        $libraryClass = self::create($library);

        // We create gets from the url like it was real
        Router::buildGets();
        // If we find a controller and the method, we call it
        if( class_exists($libraryClassName) ){
            if( method_exists($libraryClass,"build") )
                $libraryClass->build($library, $file, $folders);
        }
        else{
            if( fileexists($fullpath) ){
                $fileContents = file_get_contents( $fullpath );
                
                $cachefile = self::$cachefile;
                cache_write($cachefile, $fileContents, $optional=null);
                
                self::build($ext);
                echo $fileContents;
            }
            else{
                if( Router::get("debug") ){
                    echo "Fullpath: " . self::$fullpath;
                    echo "<br/>";
                    echo "Cache file: " . self::$cachefile;
                }
                Layout::generateError("404");
            }
        }
    }
    
    public static function create($name=null)
    {
        $name = ($name) ? $name : self::getLibrary();
        $library = Builder::checkLibrary($name);
        $classname = self::getClassName($name);
        

        if( fileexists($library) && class_in_file($classname, $library) )
        {
            self::build(self::$extension);
            
            return new $classname;
        }
        else{
            $file = self::$folder . self::getLibrary() . self::getFolders() . self::getFile();
            if( fileexists($file) ){
                self::build(self::$extension);
                $contents = file_get_contents($file);
                echo $contents;
            }
            else{
                // 404
            }
        }
    }

    public static function build($type){
        $mime = Builder::mime($type);
        
        if( !Router::get("debug") ){
            Header("Content-Type: " . $mime );
            header ("cache-control: must-revalidate");
        }
    }
    
    public static function getClassName($name=null)
    {
        $name = ($name) ? $name : self::getLibrary();
        $name = str_replace("/", "", $name);
        $classname = ucfirst($name) . "Library";
        return $classname;
    }
}

?>
