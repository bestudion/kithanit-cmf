<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Less
 *
 * @author Pityu
 */
class_alias("LessLibrary", "Less");

class LessLibrary{
    static $less, $current, $cachefile, $cachemodifiedtime;
    
    public function __construct() {
        if( !Router::get("debug") )
            error_reporting(0);
        
	self::$cachefile = Resources::$cachefile;
	self::$cachemodifiedtime = Resources::$cachemodifiedtime;
        self::$less = new lessc();
    }
    
    public static function build(){
        error_reporting(E_ALL);
        // Push the variables to the less!
        self::inject();
                
        echo self::collect();
    }
    
    public static function collect(){
        $less_folder = Builder::$less_folder;
        $less_files = $less_folder . "*.{css,less}";
        $reqfiles = glob($less_files, GLOB_BRACE);
        $tempcache = "";
        
        if( Core::isDev() && cache_skip(self::$cachefile) || !cache_exists(self::$cachefile) || self::$cachemodifiedtime > Core::getSettings("cache_timeout") ){
            foreach($reqfiles as $file){
                $filetoinclude = explode("/",$file);
                $filetoinclude = end($filetoinclude);
                $filename = explode("_",$filetoinclude);
                $filename = end($filename);

                if( fileexists($file) ){
                    $tempcache .= "/* " . $filename." */\n";
                    // $tempcache .= file_get_contents($file);
                    $tempcache .= self::minify($file);
                    
                    $tempcache .= "\n\n";
                }
                else{
                    $tempcache .= "\n/* not found " . $filename ." */\n";
                }
            }

            // Write into cache
            $tempcache = self::fixpaths($tempcache);
            cache_write( self::$cachefile, $tempcache );
        }
        else{
            // $tempcache = file_get_contents(self::$cachefile);
            $tempcache = self::fixpaths($tempcache);
            $tempcache .= self::minify(self::$cachefile);
        }
        
        
        return $tempcache;
    }
    
    public static function minify($file){
        if( Core::isDev() && Router::get("dev") ){
            $tempcache = file_get_contents($file);
        }
        else{
            self::$less = new lessc();
            // Write temp cache to the cache file
            self::inject();
            $tempcache = self::$less->compileFile($file);

            // remove tabs, consecutivee spaces, newlines, etc.
            $tempcache = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '	', '	'), '', $tempcache);
            // remove single spaces
            $tempcache = str_replace(array(" {", "{ ", "; ", ": ", " :", " ,", ", ", ";}"), array("{", "{", ";", ":", ":", ",", ",", "}"), $tempcache);   
        }     
        
        return $tempcache;
    }
    
    public static function fixpaths($tempcache){
        preg_match_all('/url\(([\s])?([\"|\'])?(.*?)([\"|\'])?([\s])?\)/i', $tempcache, $matches, PREG_PATTERN_ORDER);
        
        foreach( $matches[3] as $key=>$match ){
            $src = $match;
            $templatepath = fixpath(Builder::$template_path.$match);
            $library = ( is_image( $templatepath )) ? "resize/".Template::current()."/" : Template::current()."/";
            $cachefile = fixpath(Builder::$cache_path."/".$library.$match);

            $cacheinfo = pathinfo($cachefile);
            $rsrc = fixpath("/rsrc/".$library.$match);
            $resources = fixpath("/resources/".$library.$match);
            $isrsrc = false;

            if( is_url($src) ){
                /* if downloadable we download */
                $remotehost = returnit( parse_url($src), "host" );
                $cachefile = fixpath( returnit( pathinfo($src), "basename") );
                $cacheuri = "downloaded/".$remotehost."/".$cachefile;
                $cachefile = fixpath(Builder::$cache_path."/".$library.$cacheuri);
                $rsrc = /* Router::url("chunk") . */ fixpath("/rsrc/".$library.$cacheuri);
                $isrsrc = true;
                if( !fileexists( $cachefile ) ){
                    $cachefile = Builder::download($src, $cachefile);
                }
                else{
                    $cachefile = $cacheuri;
                }
            }
            else{
                if( fileexists($templatepath) )
                cache_write($cachefile, file_get_contents($templatepath));
            }
            
            /* if data img allowed we do so */
            if( CSS_ALWAYS_RSRC === true )
                $toreplace = $rsrc;
            else
                $toreplace = ( fileexists($cachefile) && !Core::isDev() || !is_image($cachefile) || $isrsrc === true ) ? $rsrc : $resources;
            
            if( ALLOW_CSS_DATAIMG && is_image($cachefile) )
                $toreplace = ( fileexists($cachefile) && !Core::isDev() ) ? data_img($cachefile) : $toreplace;
            
            $original = fixpath($match);
            $fullpath = realpath( fixpath(Builder::$template_path . $original) );
            
            if( $fullpath ){
                $pathinfo = pathinfo($fullpath);
                $index = $fullpath;
                 
                $preload["css"][$index]["original"] = $original;
                $preload["css"][$index]["resources"] = $resources;
                $preload["css"][$index]["rsrc"] = $rsrc;
                $preload["css"][$index]["fullpath"] = $fullpath;
                $preload["css"][$index]["basename"] = $pathinfo["basename"];
                $preload["css"][$index]["filename"] = $pathinfo["filename"];
                $preload["css"][$index]["extension"] = $pathinfo["extension"];
                
            }
            
            $tempcache = str_replace($match, $toreplace, $tempcache);
        }
        
        if( isset($preload) && is_array($preload) )
            Preload::collection($preload);
        
        return $tempcache;
    }
    
    public static function inject(){
        $template = Resources::$template;
        
        if( $template && isset($template["variables"]) ){
            $template = $template["variables"][0];
            
            self::$less->setVariables($template);
        }
        
            
    }
    
    public static function debug(){
        $arguments = ( Router::getArguments() ) ? Router::getArguments() : array();
        echo "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\"  style=\"width: 450px; border: 1px solid black;\">

            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> <a target=\"_blank\" href=\"" . Router::url("url") . "\"> URL </a> </td> </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Path:</td>
                <td height=\"30\" valign=\"middle\">" . Router::url("path") . "</td>
            </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Library:</td>
                <td height=\"30\" valign=\"middle\">" . Resources::getClassName(Resources::getLibrary()) . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Folders:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFolders() . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">File:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFile() . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Extension:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("extension") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Created:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("created") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Gets:</td>
                <td height=\"30\" valign=\"middle\">" . str_replace( ";", "<br/>", rarray( $_GET ) ) . "</td>
            </tr>
        </table>";
    }
}
?>