<?php
    $root=(isset($_SERVER['DOCUMENT_ROOT']) && $_SERVER['DOCUMENT_ROOT'] != "" ) ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER["PWD"];
    $_SERVER['DOCUMENT_ROOT'] = $root;
    $config = $_SERVER['DOCUMENT_ROOT'].'/assets/config.php';
    require_once $config;  
     
     
    $query = new Query(); 
    
    $builder = new Builder();
    $router = new Router();

    $layout = new Layout(); 
    $template = new Template();
    
    Router::setDefaultController( Core::getSettings("site_homepage") );
    Router::setDefaultAction("index");
 
    // This is the key part, we call a method that has a name stored in $stored
    Layout::init();

    if( isset($_GET["phpinfo"]) ){ print_r(phpinfo());exit; }

    ob_end_flush();   


    Query::disconnect();

?>

